/**
 * 
 * @param {*Array} arg 
 */
module.exports = function (arg) {
  if (typeof arg === 'object') {
      return Object.prototype.toString.call(arg) === '[object Array]';
  }
  return false;
}
